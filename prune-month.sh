#!/bin/sh
#GPL-3 - See LICENSE file for copyright and license details.
if ! test -f $(basename "$0");then echo "  EE run this script in its folder";exit 1;fi
if ag "^#ITP.*<loop>" "$2" > /dev/null 2>&1;then exit 0;fi
T_BUF="0"
FIRST=$(( ${1#0}  +1 )); LAST=$(date -u +"%m"); EXTRA=$(echo "$LAST 1 + p" | dc); if test "$EXTRA" = "13";then EXTRA="1";fi
if test $LAST -lt $FIRST;then SEQUENCE="$(seq $FIRST 12) $(seq 1 $LAST)";else SEQUENCE="$(seq $FIRST $LAST)";fi
SEQUENCE=$(echo $SEQUENCE $EXTRA | tr '\n' ' ')
echo "  II PRUNE_BEGIN"
for I in $SEQUENCE;do
  MONTH_TO_PRUNE=$(printf "%02i\n" $I)
  if ag "^$MONTH_TO_PRUNE\.|^\d\d\.\d\d:\d $MONTH_TO_PRUNE\.\d\d:\d @" "$2";then T_BUF="1";fi
done
if test $T_BUF = "0";then echo "  II nothing to prune"; exit 0; fi
echo "  II PRUNE_END - YOU NEED TO PRUNE TO KEEP YOUR ITP-FILE SANE - anyway ... if you abort you need to fix it by hand and restart"
echo "  II months to prune: $SEQUENCE"
echo -n "  ?? [c]-continue [a]-abort [Return] >"
read T_CONFIRM;if test "$T_CONFIRM" != "c";then echo;echo "  II aborted";exit 1;fi
echo "  ## ... just to be sure ..."
echo -n "  ?? [c]-continue [a]-abort [Return] >"
read T_CONFIRM;if test "$T_CONFIRM" != "c";then echo;echo "  II aborted";exit 1;fi
echo
for I in $SEQUENCE;do
  MONTH_TO_PRUNE=$(printf "%02i\n" $I)
  sed -i -e "/^$MONTH_TO_PRUNE\./d" -e "/^[[:digit:]][[:digit:]]\.[[:digit:]][[:digit:]]:[[:digit:]] $MONTH_TO_PRUNE\.[[:digit:]][[:digit:]]:[[:digit:]] @/d"  "$2" || exit 1
done
exit 0
