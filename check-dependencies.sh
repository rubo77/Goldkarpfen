#!/bin/sh
#GPL-3 - See LICENSE file for copyright and license details.
set -- "ag awk basename cat cmp cp cut date dc dd diff du file fold grep gunzip gzip head ls mkdir mktemp mv openssl pgrep printf ps sed seq sha512sum sort stat tail tar touch tput tr uniq wc xxd"
__WHICH(){
  ERR=
  for PROG in $1;do
    if ! command -v "$PROG" > /dev/null;then echo >&2 "  EE $PROG not found";ERR="ERROR";fi
  done
  if test "$ERR" = "ERROR";then return 1;fi
}
RE=
if test -z $EDITOR;then echo >&2 "EDITOR env var is empty.";RE="ERROR";fi
if ! command -v fzy > /dev/null && ! command -v fzf > /dev/null;then >&2 echo "  EE fzf or fzy not found";RE="ERROR";fi
if __WHICH "$1" && test "$RE" != "ERROR";then RE="ok";else echo "ERROR";exit 1;fi
if __WHICH "curl" > /dev/null;then RE="get $RE";fi
if __WHICH "darkhttpd" > /dev/null;then RE="host $RE";fi
if pgrep "^i2pd$" > /dev/null 2>&1 || nc -zv localhost 4444 > /dev/null 2>&1;then RE="i2p $RE";fi
if pgrep "^tor$" > /dev/null 2>&1 || nc -zv localhost 9050 > /dev/null 2>&1 || nc -zv localhost 9150 > /dev/null 2>&1;then RE="tor-static $RE";fi
if python3 -c "import stem" > /dev/null 2>&1;then
  if ! python3 start-hidden-service.py --test --test --test 2> /dev/null;then >&2 echo "  II cannot access tor auth cookie";echo "$RE";exit;fi
  echo "tor-ctrl ${RE#tor-static }"
else
  echo "$RE"
fi
