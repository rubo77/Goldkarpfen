#V0.67

0. ABOUT (49)

1. SPECIAL DISTINGUISHING MARK (57)

2. PLATFORM SPECIFIC INSTALL INSTRUCTIONS (67)
  2.1 OpenBSD (68)
  2.2 ANDROID/IOS AUTOMATIC INSTALL (99)
  2.3 UBUNTU QUICK INSTALL WITH TOR CONTROLLER (121)

3. SPECIAL INSTALL MODES (146)
  3.1 INSTALATION WITH ACCESS TO TOR CONTROLLER (147)
  3.2 USAGE WITH TOR_BROWSER (177)
  3.3 USAGE WITH i2pd (183)
  3.4 GITREPO AS NODE (210)
  3.5 MIXED NODES (217)
  3.6 GOPHER instead of HTTP (236)

4. BASIC MAINTENANCE (250)
  4.1 UPDATES (251)
  4.2 NODE LIST MANAGEMENT (273)
  4.3 GENERATE HTML-PAGE (293)
  4.3.1 AUTOMATIC GENERATION OF HTML-PAGE (302)
  4.4 PRUNING OF ITP-FILES (306)
  4.5 DIFF-PATCH-MODE # dependency: xdelta3 (312)
  4.6 MIGRATING TO A NEW INSTALLATION (330)
  4.7 WHITLISTING DOWNLOAD MODE (339)
  4.8 OFFER MORE THAN ONE FORK FOR DOWNLOAD (346)
  4.9 DISABLE darkhttpd LOGGING (351)

5. ADVANCED USGAE (358)
  5.1 LOOP STREAMS (359)
  5.2 MANUALLY ADDING DATA FILES (366)
  5.3 USER MADE PLUGINS (379)
  5.4 PLUGIN DEVELOPEMENT (397)
  5.5 ITP TAGS (415)
  5.6 CUSTOM SCRIPTS (428)
  5.7 SHARE HOSTING 00-ENTRIES (438)
  5.8 FORKING GOLDKARPFEN (447)

6. DISTRIBUTED SEARCH ENGINES (456)
  6.1 CONCEPTION (457)
  6.2 HOSTING A SEARCH ENGINE (486)

7. ADDENDUM (496)
_______________________________________________________________________________

# 0. ABOUT
Goldkarpfen was designed to provide a robust communication network, to secure
a share hosting platform for important Information, that are important for
                            humans
Besides other things it is a network-framework for distributed
focus-search-engines (search engines with a limited set of data)


# 1. SPECIAL DISTINGUISHING MARK
Goldkarpfen is a p2p-share-hosted daily-routine blog like communication
network. The main difference between Goldkarpfen and other systems is the calm
way of publishing: NODES WILL ONLY SYNC EACH OTHER ONCE A DAY - keep this in
mind.
Publishing (archiving) ->ONCE<- a day is advised (publishing more than one time
a day is not prohibited, because it will not cause any technical problems, but
can lead to different daily version for different users)


# 2. PLATFORM SPECIFIC INSTALL INSTRUCTIONS
## 2.1 OpenBSD
- install: coreutils gtar gsed
NOTE: you can build coreutils yourself and only build gdate and gstat

- recommended (to avoid global usage of gsed, gtar, gstat, gdate):
use a start script [¹] OR
create an extra-user OR
setup a chroot-directory

- create ~/gkbin and make sure it's in PATH (echo $PATH)

      cd ~/gkbin
      ln -s /usr/local/bin/gtar ./tar
      ln -s /usr/local/bin/gdate ./date
      ln -s /usr/local/bin/gsed ./sed
      ln -s /usr/local/bin/gstat ./stat

- edit ~/gkbin/sha512sum to contain this:

      #!/bin/sh
      echo "$(sha512 -q "$1")  $1"

- make it executable:

      chmod 755 ~/gkbin/sha512sum

- for more easy usage you may consider to install mksh
- proceed with the regular README
NOTE: tor-data-dir is: /var/tor

## 2.2 ANDROID,IOS AUTOMATIC INSTALL
android: install termux

    cp share/gki.sh ~
    cd ~
    pkg upgrade ; pkg install curl tor
    sh gki.sh "https://bitbucket.org/goldkarpfen/goldkarpfen/raw/main" "Goldkarpfen-termux.tar.gz"

ios: install ish
    cp share/gki.sh ~
    cd ~
    apk upgrade ; apk add tor curl
    sh gki.sh -a "https://bitbucket.org/goldkarpfen/goldkarpfen/raw/main" "Goldkarpfen-termux.tar.gz"

  - NOTE : if you enable also i2p support

        pkg install i2pd && i2pd --daemon --loglevel=none &

  - run gki.sh again

        sh gki.sh



- check https://gitlab.com/rubo77/Goldkarpfen/-/blob/main/README.md

- NOTE: if you enable also i2p support

      pkg install i2pd && i2pd --daemon --loglevel=none &
- run gki.sh again

      sh gki.sh

## 2.3 UBUNTU QUICK INSTALL WITH TOR CONTROLLER
NOTE: These are commands with sudo: ABSOLUTELY NO WARRANTY
GET THE LATEST Goldkarpfen-latest.tar.gz AND GO INTO THE FOLDER CONTAINING IT

    tar xfv Goldkarpfen-latest.tar.gz && cd Goldkarpfen
    sudo apt-get install build-essential python3-stem fzy silversearcher-ag tor dc xxd curl
    if ! grep "^export EDITOR" ~/.bashrc;then echo 'export EDITOR="nano"' >> ~/.bashrc ;. ~/.bashrc;fi
    mkdir -p tmp && cd tmp
    wget https://raw.githubusercontent.com/emikulic/darkhttpd/master/darkhttpd.c
    gcc darkhttpd.c -o darkhttpd; sudo mv darkhttpd /usr/local/bin
    if grep "^ *ControlPort" /etc/tor/torrc;then echo "YOU HAVE ALREADY CONIFGURED YOUR torrc - SKIP THE NEXT STEP AND CHECK IT MANUALLY";fi

THE NEXT COMMAND HAS 3 LINES

      sudo printf "ControlPort 9051\nCookieAuthentication 1\n\
      CookieAuthFile /var/lib/tor/control_auth_cookie\nCookieAuthFileGroupReadable 1\n\
      DataDirectoryGroupReadable 1\nCacheDirectoryGroupReadable 1\n" >> /etc/tor/torrc

      sudo usermod -a -G debian-tor $USER

RESTART YOUR COMPUTER - GO INTO GOLDKARPFEN FOLDER AND:

      ./check-dependencies.sh

The last line should be: tor-ctrl host get ok

      ./new-account.sh

AFTER THAT START YOUR GOLDKARPFEN WITH bash

      bash Goldkarpfen.sh


# 3. SPECIAL INSTALL MODES
## 3.1 INSTALATION WITH ACCESS TO TOR CONTROLLER
- install the basic dependencies
ag (the-silver-searcher), fzy (or fzf), libressl/openssl, xxd from vim/xxd-standalone, darkhttpd, curl, python3, python-stem, tor

- edit your /etc/tor/torrc to contain this

      ControlPort 9051
      CookieAuthentication 1
      CookieAuthFile /var/lib/tor/control_auth_cookie
      CookieAuthFileGroupReadable 1
      DataDirectoryGroupReadable 1
      CacheDirectoryGroupReadable 1

- add your user to the tor group (debian-tor on ubuntu) (restart may be neccessary!)
- test your settings

      python3 -c "import stem" && ./check-dependencies.sh

  Should return: tor-ctrl ...

- create an account

      ./new-account

- start

      bash ./Goldkarpfen.sh  # or
      mksh ./Goldkarpfen.sh  # or
      busybox sh ./Goldkarpfen.sh

- get your onion-url
Goldkarpfen will show the location of your hostname on start-up

- add your onion-url url with [r][h]

## 3.2 USAGE WITH TOR_BROWSER
- if you are using tor browser (port number 9150), add this to Goldkarpfen.config (line 5,6):

      # TOR_PORT
      9150

## 3.3 USAGE WITH i2pd
- install the basic dependencies
ag (the-silver-searcher), fzy (or fzf), libressl/openssl, xxd from vim/xxd-standalone, darkhttpd, curl, i2pd
- start your i2p-daemon

      i2pd

- test your settings

      ./check-dependencies.sh
should return: ... i2p ...

- create an account

      ./new-account
- check the config

      more ./Goldkarpfen.config
- note your SERVER_PORT

- configure a http tunnel (use SERVER_PORT defined in your Goldkarpfen.config) and restart i2pd
- get your i2p-url

      curl http://127.0.0.1:7070/?page=i2p_tunnels

- start

      bash ./Goldkarpfen.sh  # or
      mksh ./Goldkarpfen.sh  # or
      busybox sh ./Goldkarpfen.sh

- add your i2p-url with [r][h]

## 3.4 GITREPO AS NODE
- setup a git-repo in your archives folder (with ssh keys)
- after normal usage (for automation on a server see: 4.3.1):

      git add .
      git commit -m "update" --amend
      git push -f

NOTE: force push needs to be enabled for your git-repo

## 3.5 MIXED NODES
As you know, you can choose to host your node via tor or i2p (or both - adjust
your my-start-services.sh my-stop-services.sh accordingly).
If you have both tor (tor-static) and i2p running your Goldkarpfen
will download from both type of nodes.
Goldkarpfen will connect to url's reachable via tor,i2p or without proxy,
That means the following are valid urls for GK (nodes.dat example):

    http:// ... .onion
    http:// ... .i2p
    http:// ... .tld
    http://localhost:9004
    http://[fda7:404a:74ce:4242:9b7a:717:4596:8b59]:8087
    gopher://... .onion
    gopher://... .i2p
    gopher://... .tld
    gopher://localhost:9004
    gopher://[::1]:8090
    (https and ftp should also work - no warranty, not tested)

## 3.6 GOPHER instead of HTTP
NOTE: Do something similar if you want to use a custom http-server

    cp start-services.sh my-start-services.sh
    cp stop-services.sh my-stop-services.sh

- Edit my-start-service.sh: comment out darkhttpd lines and add the gopher lines:

    #darkhttpd archives/ --port $2 --daemon --log server.log ...
    geomyidae -b archives -p $2 -logfile server.log
    if test "$3" = "tor-ctrl";then python3 start-hidden-service.py $1 $2 70;else sleep 0.2;fi #80 for http, 70 for gopher

- Edit my-stop-service.sh and comment out darkhttp lines and add:

    killall geomyidae #or come up with something more elaborated if you are running multiple instances


# 4. BASIC MAINTENANCE
## 4.1 UPDATES
Normally you should update via the update plugin:
press [S] (sync_all) and [r][U] (update)

If you have no nodes you can connect to and want to invoke an update:
- get the newest Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp.tar.gz and
  Goldkarpfen-latest.tar.gz
- mv those two files into archives
- start Goldkarpfen and unpack
  Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp.tar.gz, (contains shasum)
- (advised) check the shasum (the signature has been checked on unpack)
  NOTE: This is done automatically when using [S] [r][U]
- press [r][U] (update)

If your installation is broken:
- copy Goldkarpfen-latest.tar.gz into update

      cd update
      tar -xvf Goldkarpfen-latest.tar.gz
      cp Goldkarpfen/update/sync_runtime_files.sh .
      ./sync_runtime_files.sh
      rm Goldkarpfen-latest.tar.gz

## 4.2 NODE LIST MANAGEMENT
You can add nodes to your nodes.dat via [r][a] (stream url1 + node-links).
But the management of node labels is done by editing: [!]->nodes.dat
Keep your nodes.dat nice and clean!
- example nodes.dat:

      # comment
      #_peter________#
      http://xranerunarnerunaetunatren...
      #_paul_________#
      gopher://xraar8392893naetunatren...
      #_mary_________#
      http://xraar8392893naetunatren...
      # stop sync-ALL after this

      #_barly-on1____#
      http://xraar8392893naetunatren...
      #_local________#
      http://127.0.0.1:8098

## 4.3 GENERATE HTML-PAGE
- press [H]
# NOTE: you can edit/add your launher.dat to change the default cmd
# (last entry counts) e.g.

    sh ./generate-html.sh --days=23 --out=archives/index.html

your html-page will be available via tor-browser/browser-with-i2p-proxy
under your url1 (http://xyz...abc.{onion,i2p})

### 4.3.1 AUTOMATIC GENERATION OF HTML-PAGE
- sync-from-nodes.sh --loop calls every "Xth" iteration:

      ./x_sync-cycle-X.sh # <- ./generate-html.sh --out=archives/index.html

## 4.4 PRUNING OF ITP-FILES
itp-files contain yearly rotational data. That means that they have to be
pruned on a regular base. Goldkarpfen prunes old entries on the first of every
month. It will you inform on startup on that day. Check carfully and backup
some entries you wanna keep (reposting)

## 4.5 DIFF-PATCH-MODE # dependency: xdelta3
If you have xdelta3 installed, GK will produce a diff-file and offers it for
download - there is no user interaction needed, generation and processing of
diffs works automatically.

Other nodes will download the diff file (if they have the according archive)
and patch the according archive - this reduces the used bandwidth significantly
(~500 B instead of 1-318 kBs per sync)

GK will only produce and keep one diff (from the last archive) -
cascading-diffs are not supported: the recieving user needs to have the last
archive to process a diff ; if the "gap" is greater, GK will just download the
whole file again.

Diffs can only be generated if your archive was created ONCE a day; GK will
take care of that and block generation of diffs, if your archive has been
created more than once a day.

## 4.6 MIGRATING TO A NEW INSTALLATION
If you want to migrate your data to a fresh install you have to do:
- unpack the latest Goldkarpfen
- copy .keys/ Goldkarpfen.config my-start-services.sh my-stop-services.sh
  nodes.dat blacklist.dat; and user made plugins in plugins/
- copy your itp-files {.itp .itp.sha512sum .itp.sha512sum.sig}
- copy archives/*.tar.gz
- start Goldkarpfen and unpack your favorite archives

## 4.7 WHITLISTING DOWNLOAD MODE
- create a file in your Goldkarpfen folder called whitelist.dat
- edit it and add at least:
    Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp.tar.gz
    Goldkarpfen-latest.tar.gz
- add more streams of your choice

## 4.8 OFFER MORE THAN ONE FORK FOR DOWNLOAD
- update to 279+
- edit update-provider.inc.sh and have a line like this:
    LISTING_REGEXP="Goldkarpfen-latest\.tar\.gz|Goldkarpfen-termux\.tar\.gz"

## 4.9 DISABLE darkhttpd LOGGING
darkhttpd will log to server.log,
if no log file is specified it would log to stdout.
- disable logging:
rm server.log ; ln -s /dev/null ./server.log


# 5. ADVANCED USGAE
## 5.1 LOOP STREAMS
You can use your itp-stream as a rotational yearly infinite loop.
This is for creating artsy tao-like books, that can be read again and again.
Add a <loop> tag to your first line in your itp-file. Looped streams get not pruned.
IMPORTANT: do not comment non-loop streams (default: disabled)
- blacklisting of loop-streams not following this etiquette is advised

## 5.2 MANUALLY ADDING DATA FILES
itp-files:
- exit Goldkarpfen

      cp alias-_key_addr_.itp itp-files/
      cp alias-_key_addr_.itp itp-files.sha512sum itp-files/
      cp alias-_key_addr_.itp itp-files.sha512sum.sig itp-files/

- start Goldkarpfen

archives: (works from a running Goldkarpfen)

      cp alias-_key_addr_.itp.tar.gz quarantine/
- (optionally) unpack the archive with [u]-unpack
- use [m]-quarantine to move it into archives

## 5.3 USER MADE PLUGINS
- host your plugins in archives/share
- line 2 must contain the version, line 3 the verification stream (your stream)
  (with leading #)

Example (line 1 - 3):

      # <!-- License info // plugin description -->
      #V0.1
      #Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp

- add the sha512sum as a post to your stream
Example:
 
       eea346f85c6f9e08079a80355a069d327993b480b22cd02dccbc3ceae27230fc0734321443543094f51a8952580466210c0a76d73b662ed2d629e4eb5657fee1  example-plugin.sh

- add a post with the plugin tag (mandatory format <...> #... #...)
Example:

      <plugin=share/example-plugin.sh> #Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp #V0.1

## 5.4 PLUGIN DEVELOPEMENT
Please remember - all variables in POSIX-SHELL are GLOBAL!
(except vars exclusively defined in a while loop or a subshell)
Goldkarpfen uses this globals:

GENERAL:

    GK_ACTIVE_FILES GK_ALIAS GK_COLS GK_DATE GK_DIFF_MODE GK_FZF_CMD GK_ID GK_JM GK_LN GK_MODE GK_PATH GK_READ_CMD GK_TOR_PORT GK_URL_REGEXP
    OWN_ADDR OWN_ALIAS OWN_STREAM OWN_SUM
    ITPFILE UPD_NAME UPD_NAME_REGEXP VERIFICATION_STREAM
    USER_HOOK_ARCHIVE_START USER_HOOK_START USER_PLUGINS_MENU USER_QUIT

SCRATCH VARIABLES (do not call functions in between definition and processing):

    T_BUF{,1,2,3,4,5} T_CMD T_CONFIRM T_CHAR T_COUNTER T_FILE T_LINE

You should avoid using new globals at any cost
(use set, subshells or scratch-variables where ever possible).
If you want to use some globals, add a uniq Prefix (e.g. $MYPLUGIN_XYZ)

## 5.5 ITP TAGS
Goldkarpfen uses the following reserved tags:

    <url1=[url]> # main url of itp-stream
    <exec=[shell-code]> # for snippets
    <node=[node-url]> # GK-node-url
    <plugin=[path]> # path relative to "archives"
    <download=[path]> # path relative to "archives"
    <e-download=[external-url]> # external url download
    <url=[url]> # external url

Beside those you can define your own tags to be used in plugins.

## 5.6 CUSTOM SCRIPTS
- instead of the default scripts you can use:
my-sync-from-nodes.sh
my-check-dependencies.sh
my-start-services.sh
my-stop-services.sh

- you can add your own include in which you can overwrite any function:
my-include.sh

## 5.7 SHARE HOSTING 00-ENTRIES
You can share host plugins and other downloads. plugin.sh and download.sh will
echo the format for it. If you want to add persistent entries of this technical
data, that will not be pruned, you can use 00-entries. Have a look at
`itp-files/Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp`.
The 00 entries have to be edited manually ([!]-edit) - both, posts and
comments, are allowed.
BE SURE THAT THOSE ENTRIES ARE ABOVE ALL "real-date-entries" AND IN ORDER.

## 5.8 FORKING GOLDKARPFEN
- edit update-provider.inc.sh and change the entries - use your itp-file as the
  verification stream
- pack a tarball with your forked Goldkarpfen and add a checksum to your
  itp-file (like in: Goldkarpfen-1JULSJ5Nnba9So48zi21rpfTuZ3tqNRaFB.itp)
- move that tarball into your archives with your itp-file.tar.gz
- tell your peers to change their update-provider.inc.sh accordingly


# 6. DISTRIBUTED SEARCH ENGINES
## 6.1 CONCEPTION
- 1 out of (X) nodes hosts a (http/gopher) search engine,
  which holds a database of its archive (+ manually curated search entries);
  usable via curl (like url?search=what1+what2) to be used from inside GK
- each search engine mirrors search data of (Y) search engines
  (with minimal archive overlap) (=> database size Y*30MB)
- each client search request goes to (Z) random search engines
  the results will be merged/sorted on the client side
- important data gets mirrored (share hosted by other nodes) by J% of the nodes
- X=60 ; Y=10-20 ; Z=5 and J>2 for important data would result in good
  search results for important data

LEVEL 0 SEARCH ENGINE
a LVL0 search engine consist of:
  the search engine with indexed data of:
  + itp-streams with original content (blogs)
  + direct downloads/plugins of important content in the context of the search
    engine hoster
  + downlinks of important content in the context of the search engine hoster
  + entries of LVL1 search_engines (LVL0 search engines should de-list
    search engines that does not meet the requirements for LVL1)

LEVEL 1 SEARCH ENGINE
a LVL1 search engine consist of:
  the search engine with indexed data of:
  + itp-streams of downlink collections

(NOTE: downlink example <url=https://gnu.org>)

## 6.2 HOSTING A SEARCH ENGINE
- add a gopher hidden-service/i2p-tunnel node and start geomyidae along http
- mkdir -p archives/gopher/data
- add some itp-files (or .txt/.dat) to data and add an info-suffix, e.g.:
    mv xyz.itp 'xyz.itp http_url-of-that-file.onion'
- add a search-plugin compatible search.cgi to archives/gopher/data [²]
- change/add features of our search.cgi (search algorithm,database-backend,...)
- announce your search engine in your itp-stream like this:
    <search_engine=gopher://---your-url---.onion/gopher/search.cgi? #  my-search (LVL0)>

# 7. ADDENDUM
[¹] openBSD start-gk.sh:

    #!/bin/sh
    export PATH="$HOME/gkbin:$PATH"
    for FFF in date tar sed stat;do
      if ! test $(basename $(realpath $(command -v $FFF))) = "g$FFF";then
        echo "install: gstat, gdate (coreutils) and gtar, gsed"
        exit
      fi
    done
    cd ~/Goldkarpfen || exit
    mksh Goldkarpfen.sh

[²] simple search engine:

    #!/bin/sh
    #V0.4
    #GPL-3 - See LICENSE file for copyright and license details.
    # The original Goldkarpfen search engine
    cd __search.cgi_path__ || exit 1
    # edit __search.cgi_path__ accordingly
    set -- "${2%&page=*}" "$(echo "$2" | ag -o "page=\d*$")"
    #echo "search: $1 $2"
    if test "$(printf "%i" "${2#page=}")" = "${2#page=}";then PAGE=${2#page=};else PAGE=;fi
    if test -z "$PAGE";then
      LINE1=1
      LINE2=10
    else
      PAGE=$(echo "$PAGE 0 + p" | dc)
      LINE1=$(echo "$PAGE 10 * 1 + p" | dc)
      LINE2=$(echo "$LINE1 9 + p" | dc)
      set -- "$(echo "$1" | sed "s/ P$PAGE$//")"
    fi
    ag --silent -m 3 --heading --nonumbers "$1" data/*.itp | grep -v '^$' | sed -n "$LINE1,$LINE2 p"
    # or use a list of itp-files instead of *.itp (or .txt/.dat files)
    # NOTE: designed to work with for 15 search streams
    exit 0

{LICENSE:CC0}
