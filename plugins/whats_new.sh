#GPL-3 - See LICENSE file for copyright and license details.
USER_PLUGINS_MENU="[w]-wassup:__USER_WHATSNEW $USER_PLUGINS_MENU"
__DDD(){
  while NFS= read LLL;do
    echo "- $LLL"
  done
}
__USER_WHATSNEW(){
  T_BUF="$(seq 0 7 | __DDD | $GK_FZF_CMD | __collum 2 )"
  echo $T_BUF
  if test -z "$T_BUF";then echo "  II empty";return;fi
  cd itp-files || exit
  T_BUF="$(date +'%m.%d' -d "@$(($(date +%s) - $((86400 * $T_BUF))))")"
  T_BUF2="^#LICENSE:CC0 $(date +%y)-("${GK_DATE%%.*}"|"${T_BUF%%.*}")";T_BUF3="$(ag -l "$T_BUF2|^#ITP.*<loop>" *.itp)"
  echo "##### POSTS #####"
  ag --noheading --nonumbers "^$T_BUF:\d " $T_BUF3 | ag -v "^.*:\d\d\.\d\d:\d \d\d\.\d\d:\d @" | grep -v '^$' |
  while IFS= read -r T_LINE;do
    # alias text
    set -- "${T_LINE%%-*}" "${T_LINE##*itp:}"
    echo "$(tput rev)[$1]$(tput sgr0) $2" | sed 's/\&bsol;/\\/g' |fold -w "$GK_COLS" -s
  done
  printf "##### COMMENTS #####\n"
  ag --noheading --nonumbers "^$T_BUF:\d \d\d\.\d\d:\d @" $T_BUF3 | grep -v '^$' |
  while IFS= read -r T_LINE;do
    # alias1 alias2
    set -- "${T_LINE%%-*}" "$(ag "$(echo "${T_LINE#*@}" | __collum 1 )" ../cache/aliases | __collum 1)"
    if ! test -z "$2" || test "$1" = "$OWN_ALIAS";then echo "$T_BUF [$1] -> $(tput rev)[$2]$(tput sgr0) ${T_LINE#*@?????????????????????????????????? }" | sed 's/\&bsol;/\\/g' | fold -w "$GK_COLS" -s;fi
  done
  cd .. || exit
}
