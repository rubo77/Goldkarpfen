./generate-html.sh --out=archives/index.html

OWN_STREAM="itp-files/$(sed -n '2p' Goldkarpfen.config)"
OWN_ALIAS=$(basename "$OWN_STREAM");OWN_ALIAS=${OWN_ALIAS%%-*}
URL="$(head -n 1 $OWN_STREAM | grep '<url1=.'|cut -d= -f2|cut -d'>' -f1)"

if ! curl --progress-bar -f --proxy if ! curl --progress-bar -f --proxy socks5://127.0.0.1:9050 --socks5-hostname 127.0.0.1:9050 $URL > /dev/null;then
  if ! curl --progress-bar -f --proxy socks5://127.0.0.1:9050 --socks5-hostname 127.0.0.1:9050 $URL > /dev/null;then
    echo restarting tor
    sudo systemctl restart tor
  fi
fi
